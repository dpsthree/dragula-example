import { DragulaDemoPage } from './app.po';

describe('dragula-demo App', () => {
  let page: DragulaDemoPage;

  beforeEach(() => {
    page = new DragulaDemoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
