import { Component, ViewChild, ElementRef } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  staff1 = ['ITEM 1', 'ITEM 2', 'ITEM 3', 'ITEM 4'];
  staff2 = ['ITEM 5', 'ITEM 6', 'ITEM 7', 'ITEM 8'];
  participants1 = ['part1', 'part2', 'part3'];
  participants2 = ['part4', 'part5', 'part6'];
  title = 'app';
  containerList = [{ list: this.staff1, parts: this.participants1 }, { list: this.staff2, parts: this.participants2 }];
  @ViewChild('entry') roomEntry: ElementRef;

  constructor(private dragulaService: DragulaService) {
    dragulaService.setOptions('first-bag', {
      removeOnSpill: true
    });
  }

  checkIt() {
    console.log('list1', this.staff1);
    console.log('list2', this.staff2);
  }
}
