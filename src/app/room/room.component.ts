import { Component, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent {
  @Input() list;
  @Input() parts;
  @ViewChild('staff') staff: ElementRef;
  @ViewChild('participants') participants: ElementRef;
  @Output() targetListChangedEmitter = new EventEmitter<any>();
  @Output() sourceListChangedEmitter = new EventEmitter<any>();
  id: number
  constructor(dragulaService: DragulaService) {
    dragulaService.dropModel.subscribe(value => {
      const toMeStaff = this.staff.nativeElement === value[2]
      const fromMeStaff = this.staff.nativeElement === value[3]
      const toMePart = this.participants.nativeElement === value[2]
      const fromMePart = this.participants.nativeElement === value[3]
      if (toMeStaff) {
        console.log('dropped on me', value, this.staff, this.staff.nativeElement);
        this.targetListChangedEmitter.emit({list: this.list, room: this.id});
      } else if (fromMeStaff) {
        console.log('removed from me', value, this.staff, this.staff.nativeElement);
        this.sourceListChangedEmitter.emit({ list: this.list, room: this.id });
      }
    })
    dragulaService.removeModel.subscribe(value => {
      const fromMe = this.staff.nativeElement === value[2]
      if (fromMe) {
        console.log('removed from me', value, this.staff, this.staff.nativeElement);
        this.sourceListChangedEmitter.emit({ list: this.list, room: this.id });
      }
    })
  }
}

