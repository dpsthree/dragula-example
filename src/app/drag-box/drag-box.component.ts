import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-drag-box',
  templateUrl: './drag-box.component.html',
  styleUrls: ['./drag-box.component.css']
})
export class DragBoxComponent {
  @Input() textVal: string

}
